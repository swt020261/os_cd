
==========
關於本光碟
==========

\Tinix:	書中所附程式

	其中很多目錄中除了包含源代碼(*.asm, *.inc, *.c, *.h)外，還有這樣一些檔案:

	boot.bin	開機磁區(Boot Sector)，可透過 FloppyWriter 寫入軟碟(或軟碟映像)。
	loader.bin	LOADER，直接拷貝至軟碟(或軟碟映像)根目錄。
	kernel.bin	內核(Kernel)，直接拷貝至軟碟(或軟碟映像)根目錄。

	bochsrc.bxrc	Bochs 配置檔案，如果系統中安裝了 Bochs-2.1.1 可直接雙擊執行。其它細節請見書第 2.7 節。
	godbg.bat	測試時可使用此批處理檔案。它假設 Bochs-2.1.1 安裝在 D:\Program Files\Bochs-2.1.1\ 中。
	TINIX.IMG	軟碟映像。可直接透過 Bochs 或者 Virtual PC 營運。

	*.com		可以在 DOS (必須為純 DOS) 下執行的檔案。


\Tools:	一些小工具 (在 VC6 下編譯透過)

	DescParser	描述符分析器，輸入描述符的值，可以得出起基址、界限、屬性等訊息。

	ELFParser	ELF 檔案分析器，可以列出一個 ELF 檔案的 ELF Header、 Program Header、Section Header 等訊息。

	FloppyWriter	用以寫引導扇區，支援軟碟和軟碟映像。

	KrnlChecker	用以檢查一個 Tinix 內核加載後位置是否正確。

